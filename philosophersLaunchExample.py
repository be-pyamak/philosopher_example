from pyAmakIHM.classes.fenetre import Fenetre
from controleurPhilosophersExample import ControleurPhilosophersExample
from philosophersAmasExample import PhilosophersAmasExamples
from threading import Thread

fenetre = Fenetre("Prototype Philosophers")
amas = PhilosophersAmasExamples()

controleur = ControleurPhilosophersExample(fenetre, amas)


def run():
    controleur.get_amas().run()


def main():
    controleur.initialisation()
    Thread(target=run).start()
    controleur.get_fenetre().display()


main()
