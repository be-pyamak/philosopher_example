from pyAmakIHM.classes.controleur import Controleur
from state import State
from math import cos, sin, pi


class ControleurPhilosophersExample(Controleur):

    def __init__(self, fenetre, amas):
        super().__init__(fenetre, amas)
        self.__philosophers = []
        self.__numberPhilosopher = 10
        self.__barChart = []
        self.__barChart.append(self.addBarChart('Eaten Pastas'))
        self.__barChart.append(self.addBarChart('Hours of tkinking'))
        self.__barChart.append(self.addBarChart('Hours of hunger'))

    def initialisation(self):

        widthCanvas = self.get_fenetre().get_canvas_width()
        heightCanvas = self.get_fenetre().get_canvas_height()


        # Init
        self.setTitle(self.__barChart[0],'Eaten Pastas')
        self.setXLabel(self.__barChart[0],'Philosophers')
        self.setYLabel(self.__barChart[0],'Number of eaten pastas')

        self.setTitle(self.__barChart[1],'Hours of thinking')
        self.setXLabel(self.__barChart[1],'Philosophers')
        self.setYLabel(self.__barChart[1],'Hours')

        self.setTitle(self.__barChart[2],'Hours of hunger')
        self.setXLabel(self.__barChart[2],'Philosophers')
        self.setYLabel(self.__barChart[2],'Hours')

        for i in range(self.__numberPhilosopher):
            x = 100 * cos(2 * pi * i / self.__numberPhilosopher) + (widthCanvas / 2)
            y = 100 * sin(2 * pi * i / self.__numberPhilosopher) + (heightCanvas / 2)

            carre = self.draw_rectangle(x, y, 20, 20, 'green')
            self.draw_rectangle(x - 15, y, 20, 7, 'black')

            self.__philosophers.append(carre)

            nom = 'Mr ' + str(i)
            self.addColumn(self.__barChart[0],nom)
            self.addColumn(self.__barChart[1],nom)
            self.addColumn(self.__barChart[2],nom)

    def updateCycle(self, env, amas):
        agents = amas.get_Agents_Sorted()

        for i in range(10):
            state = agents[i].get_state()
            if state == State.EATING:
                self.change_color(self.__philosophers[i], 'green')
                self.increaseValue(self.__barChart[0],i, 1)

            elif state == State.HUNGRY:
                self.change_color(self.__philosophers[i], 'red')
                self.increaseValue(self.__barChart[2],i, 1)

            else:
                self.change_color(self.__philosophers[i], 'blue')
                self.increaseValue(self.__barChart[1],i, 1)

            coords = self.get_coords_element(self.__philosophers[i])
            if agents[i].get_Left_Fork().owned(agents[i]):
                self.draw_rectangle(coords[0] - 15, coords[1], 20, 7, 'black')
            else:
                self.draw_rectangle(coords[0] - 15, coords[1], 20, 7, 'white')

            if agents[i].get_Right_Fork().owned(agents[i]):
                self.draw_rectangle(coords[0] + 15, coords[1], 20, 7, 'black')
            else:
                self.draw_rectangle(coords[0] + 15, coords[1], 20, 7, 'white')
